'use strict';
console.log('ejercicio 5');

const episodes = async () => {
    try {
        const response = await fetch('https://rickandmortyapi.com/api/episode');
        const { info, results } = await response.json();

        const episodes = [...results];
        let { next } = info;
        while (next !== null) {
            const response = await fetch(next);
            const { info, results } = await response.json();
            episodes.push(...results);
            next = info.next;
        }

        const h = episodes.filter((episodes) =>
            episodes.air_date.includes('January')
        );

        console.log(h);

        let personajesEnero = [];
        for (const personajes of h) {
            personajesEnero.push(...personajes.characters);
        }
        // const person = personajesEnero[0].concat(
        //     personajesEnero[1],
        //     personajesEnero[2]
        // );NO HACER LINEA 26 SPREAD OPERATOR
        const hh = [];
        for (const p of personajesEnero) {
            const char= await getCharacters(p)
            hh.push(char);
        }

        return hh;
    } catch (error) {
        console.log('error');
    }
};

const getCharacters = async (URL) => {
    try {
        const response = await fetch(URL);
        const { name } = await response.json();
        console.log(name);

        return name;
    } catch (error) {
        console.log('personaje error');
    }
};
episodes();


episodes().then((data)=>{
    console.log(data);
})
