'use strict';

console.log('autoevaluación ejercicio 2');
let count = 0;
let minutes = 59;
let hours = 0;
let days = 0;

setInterval(() => {
    count += 30;

    /*-----------------------------------*/
    if (count > 59) {
        minutes++;
        count = 0;
    } else if (minutes > 59) {
        hours++;
        minutes = 0;
    } else if (hours > 23) {
        days++;
        hours = 0;
    }

    console.log(
        `${days} días, ${hours} hora, ${minutes} minutos, ${count} segundos.`
    );
}, 5000);
