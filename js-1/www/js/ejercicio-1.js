'use strict';

console.log('hola mundo');

const randomUser = async (numUser) => {
    try {
        const queryURL = 'https://randomuser.me/api/';
        const response = await fetch(queryURL + `?results=${Number(numUser)}`);
        const { results } = await response.json();
        const usersData = [];
        for (const user of results) {
            const { first, last } = user.name;
            const { gender } = user;
            const { country } = user.location;
            const { email } = user;
            const { large } = user.picture;

            usersData.push({ first, last, gender, country, email, large });
        }
        console.log(usersData);
    } catch {
        console.log('error');
    }
};

console.log(randomUser(1));
