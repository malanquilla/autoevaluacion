'use strict';

console.log('ejercicio 4');
const names = [
    'A-Jay',
    'Manuel',
    'Manuel',
    'Eddie',
    'A-Jay',
    'Su',
    'Reean',
    'Manuel',
    'A-Jay',
    'Zacharie',
    'Zacharie',
    'Tyra',
    'Rishi',
    'Arun',
    'Kenton',
];

// const noRepeat = names.reduce((acc, element) => {
//     if (!acc.includes(element)) {
//         acc.push(element);
//     }
//     return acc;
// }, []);


//slash ast ast tabulador
/**
 * 
 * @param {Array} array - es un array
 * @returns array con los nombres no repetidos
 */
function noRepeat(array) {


   return array.reduce((acc, element) => {
        if (!acc.includes(element)) {
            acc.push(element);
        }
        return acc;
    }, []);
    
    
}

console.log(noRepeat(names));
